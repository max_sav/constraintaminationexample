package com.example.constraintlayoutanimationexample.ui.main

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.constraintlayout.widget.ConstraintSet
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.get
import androidx.transition.TransitionManager
import com.example.constraintlayoutanimationexample.R
import com.example.constraintlayoutanimationexample.databinding.MainFragmentBinding

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val markers = arrayOf(
        R.id.marker1,
        R.id.marker2,
        R.id.marker3,
        R.id.marker4,
        R.id.marker5,
        R.id.marker6,
        R.id.marker7
    )

    private val viewModel: MainViewModel by lazy(mode = LazyThreadSafetyMode.NONE) {
        ViewModelProviders.of(this, MainViewModelFactory(markers.size))
            .get<MainViewModel>()
    }

    private lateinit var binding: MainFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.bind(view)!!
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.setLifecycleOwner(viewLifecycleOwner)
        binding.viewModel = viewModel

        viewModel.position.observe(viewLifecycleOwner,
            Observer {
                setConstraints(markers[it])
                TransitionManager.beginDelayedTransition(binding.main)
            }
        )
    }

    override fun onDestroyView() {
        binding.unbind()
        super.onDestroyView()
    }

    private fun setConstraints(@IdRes marker: Int) {
        ConstraintSet().apply {
            clone(binding.main)
            connect(R.id.item1, ConstraintSet.START, marker, ConstraintSet.START)
            connect(R.id.item1, ConstraintSet.END, marker, ConstraintSet.START)
            connect(R.id.item1, ConstraintSet.TOP, marker, ConstraintSet.BOTTOM)
            connect(R.id.item1, ConstraintSet.BOTTOM, marker, ConstraintSet.BOTTOM)
            applyTo(binding.main)
        }
    }
}
