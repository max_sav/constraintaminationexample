package com.example.constraintlayoutanimationexample.ui.main

import androidx.annotation.IntRange
import androidx.lifecycle.*

class MainViewModel(private val count: Int) : ViewModel() {

    @IntRange(from = 0)
    private var positionInternal = 0
        set(value) {
            field = value
            _position.postValue(value)
        }

    private var _position = MutableLiveData<Int>()
    val position: LiveData<Int> get() = _position

    fun next() {
        positionInternal = (positionInternal + 1) % count
    }

    fun prev() {
        positionInternal = ((positionInternal - 1).takeIf { it >= 0 } ?: count-1) % count
    }
}


@Suppress("UNCHECKED_CAST")
class MainViewModelFactory(@IntRange(from = 1) private val count: Int) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>) =
        MainViewModel(count = count) as T
}